#pragma once

#include <iostream>
#include <Windows.h>
#include <string>
#include <sstream>
#include <mutex>
#include <initializer_list>

/*
	This single header logger library is meant to be a lightweight, single
	threaded logging library to provide some helpful formatting trickery
	using tempaltes in order to create fast and easy to write logging
	messages. Just add the header file into your code base and you are
	ready to go. Almost. The only other requirement is that if wish to
	use a type in the logging messages, there must be a definition of
	std::to_string which accepts it.

	Integrating a custom type into the logger:
	Given a class Vector2f which is declared as follows:
	class Vector2f {
	public:
		float x;
		float y;

		Vector2f(float x, float y) : x(x), y(y) {}
	};
	In order to use the class in composition with the logger, one
	must implement std::string std::to_string(const Vector2f& vec).
	this can be done fairly simply:
	namespace std {
		std::string to_string(cosnt Vector2f& vec) {
			return "(" + std::to_string(vec.x) + ", " + std::to_string(vec.y) + ")";
	}
	In case your class implements the ILoggable interface using public
	inheritence, there is no need to manually define an override for 
	std::string std::to_string(const Vector2f& vec) on your own.

	Log level:
	From most severe to least severe
	-Error (red)
	-Warning (yellow)
	-Confirm (green)
	-Log (white)
*/

class ILoggable {
public:
	virtual std::string to_string() = 0;

	operator ILoggable*() const {
		return (ILoggable*)this;
	}
};

struct LogContext {
	bool prefix = false;
	int max_characters = 256;
};

// implementation of std::to_string for std::string, ILoggable and floats
namespace std {
	std::string to_string(const std::string& str) {
		return str;
	}

	std::string ftos(const float& f) {
		std::ostringstream  os;
		os << f;
		return os.str();
	}

	std::string to_string(ILoggable* loggable) {
		return loggable->to_string();
	}
}

#pragma region log level
#define ALL_LOG 4
#define CONFIRM_LOG 3
#define WARN_LOG 2
#define ERROR_LOG 1
#define NO_LOG 0
#define LOG_LEVEL ALL_LOG

/* 
	if set to any value other than 1, the log level will not be displayed 
	if a macro is set to 1, the log level may still not be display,
	depending on the LOG_LEVLE macro's value
*/
#define DO_LOG 1
#define DO_CONFIRM 1
#define DO_WARN 1
#define DO_ERROR 1

#if DO_ERROR == 1
	#if LOG_LEVEL > NO_LOG
	#define ERROR1BODY {\
		Logger::print(std::to_string(arg), ERROR_COLOUR);\
	}
	#define ERROR2BODY {\
		if (context.max_characters > 0)\
			Logger::max_print(context.prefix ? ERROR_PREFIX : "", std::to_string(str), context.max_characters, ERROR_COLOUR);\
		else\
			Logger::print((context.prefix ? ERROR_PREFIX : "") + std::to_string(str), ERROR_COLOUR);\
	}
	#define ERROR3BODY {\
		std::string out = Logger::resolve_pattern(pattern, args...);\
		Logger::print(out, ERROR_COLOUR);\
	}
	#define ERROR4BODY {\
		std::string out = Logger::resolve_pattern(pattern, args...);\
		if (context.max_characters > 0)\
			Logger::max_print(context.prefix ? ERROR_PREFIX : "", out, context.max_characters, ERROR_COLOUR);\
		else {\
			if (context.prefix) out = ERROR_PREFIX + out;\
			Logger::print(out, ERROR_COLOUR);\
		}\
	}
	#else
	#define ERROR1BODY {}
	#define ERROR2BODY {}
	#define ERROR3BODY {}
	#define ERROR4BODY {}
	#endif
#else
#define ERROR1BODY {}
#define ERROR2BODY {}
#define ERROR3BODY {}
#define ERROR4BODY {}
#endif

#if DO_WARN == 1
	#if LOG_LEVEL > ERROR_LOG
	#define WARN1BODY {\
		Logger::print(std::to_string(arg), WARN_COLOUR);\
	}
	#define WARN2BODY {\
		if (context.max_characters > 0)\
			Logger::max_print(context.prefix ? WARN_PREFIX : "", std::to_string(str), context.max_characters, WARN_COLOUR);\
		else\
			Logger::print((context.prefix ? WARN_PREFIX : "") + std::to_string(str), WARN_COLOUR);\
	}
	#define WARN3BODY  {\
		std::string out = Logger::resolve_pattern(pattern, args...);\
		Logger::print(out, WARN_COLOUR);\
	}
	#define WARN4BODY {\
		std::string out = Logger::resolve_pattern(pattern, args...);\
		if (context.max_characters > 0)\
			Logger::max_print(context.prefix ? WARN_PREFIX : "", out, context.max_characters, WARN_COLOUR);\
		else {\
			if (context.prefix) out = WARN_PREFIX + out;\
			Logger::print(out, WARN_COLOUR);\
		}\
	}
	#else
	#define WARN1BODY {}
	#define WARN2BODY {}
	#define WARN3BODY {}
	#define WARN4BODY {}
	#endif
#else
#define WARN1BODY {}
#define WARN2BODY {}
#define WARN3BODY {}
#define WARN4BODY {}
#endif

#if DO_CONFIRM == 1
	#if LOG_LEVEL > WARN_LOG
	#define CONFIRM1BODY {\
		Logger::print(std::to_string(arg), CONFIRM_COLOUR);\
	}
	#define CONFIRM2BODY {\
		if (context.max_characters > 0)\
			Logger::max_print(context.prefix ? CONFIRM_PREFIX : "", std::to_string(str), context.max_characters, CONFIRM_COLOUR);\
		else\
			Logger::print((context.prefix ? CONFIRM_PREFIX : "") + std::to_string(str), CONFIRM_COLOUR);\
	}
	#define CONFIRM3BODY {\
		std::string out = Logger::resolve_pattern(pattern, args...);\
		Logger::print(out, CONFIRM_COLOUR);\
	}
	#define CONFIRM4BODY {\
		std::string out = Logger::resolve_pattern(pattern, args...);\
		if (context.max_characters > 0)\
			Logger::max_print(context.prefix ? CONFIRM_PREFIX : "", out, context.max_characters, CONFIRM_COLOUR);\
		else {\
			if (context.prefix) out = CONFIRM_PREFIX + out;\
			Logger::print(out, CONFIRM_COLOUR);\
		}\
}
	#else
#define CONFIRM1BODY {}
#define CONFIRM2BODY {}
#define CONFIRM3BODY {}
#define CONFIRM4BODY {}
	#endif
#else
#define CONFIRM1BODY {}
#define CONFIRM2BODY {}
#define CONFIRM3BODY {}
#define CONFIRM4BODY {}
#endif

#if DO_LOG == 1
	#if LOG_LEVEL > CONFIRM_LOG
	#define LOG1BODY {\
		Logger::print(std::to_string(arg), LOG_COLOUR);\
	}
	#define LOG2BODY {\
		if (context.max_characters > 0)\
			Logger::max_print(context.prefix ? LOG_PREFIX : "", std::to_string(str), context.max_characters, LOG_COLOUR);\
		else\
			Logger::print((context.prefix ? LOG_PREFIX : "") + std::to_string(str), LOG_COLOUR);\
	}
	#define LOG3BODY {\
		std::string out = Logger::resolve_pattern(pattern, args...);\
		Logger::print(out, LOG_COLOUR);\
	}
	#define LOG4BODY {\
		std::string out = Logger::resolve_pattern(pattern, args...);\
		if (context.max_characters > 0)\
			Logger::max_print(context.prefix ? LOG_PREFIX : "", out, context.max_characters, LOG_COLOUR);\
		else {\
			if (context.prefix) out = LOG_PREFIX + out;\
			Logger::print(out, LOG_COLOUR);\
		}\
	}
	#else
	#define LOG1BODY {}
	#define LOG2BODY {} 
	#define LOG3BODY {}
	#define LOG4BODY {}
	#endif
#else
#define LOG1BODY {}
#define LOG2BODY {} 
#define LOG3BODY {}
#define LOG4BODY {}
#endif
#pragma endregion

#pragma region log colour
const unsigned char CONSOLE_RED = 4;
const unsigned char CONSOLE_YELLLOW = 6;
const unsigned char CONSOLE_GREEN = 2;
const unsigned char CONSOLE_WHITE = 15;

const unsigned char ERROR_COLOUR = CONSOLE_RED;
const unsigned char WARN_COLOUR = CONSOLE_YELLLOW;
const unsigned char CONFIRM_COLOUR = CONSOLE_GREEN;
const unsigned char LOG_COLOUR = CONSOLE_WHITE;
const unsigned char DEFUALT_CONSOLE_COLOUR = CONSOLE_WHITE;
#pragma endregion

#pragma region log prefixes
const std::string ERROR_PREFIX = "Error: ";
const std::string WARN_PREFIX = "Warning: ";
const std::string CONFIRM_PREFIX = "Confirm: ";
const std::string LOG_PREFIX = "Log: ";
#pragma endregion

#pragma region multi threading
// define as 1 to activate multi threaded log, set to any other value to turn off
#define MULTI_THREADED_LOG 0

#if MULTI_THREADED_LOG == 1
#define LOG_LOCK() Logger::get_console_mutex().lock
#define LOG_RELEASE() Logger::get_console_mutex().unlock
#else
#define LOG_LOCK()
#define LOG_RELEASE()
#endif
#pragma endregion

class Logger {
public:
	static void init() {
		HANDLE h_console = Logger::get_console_handle();
		SetConsoleTextAttribute(h_console, 4);  // red
		SetConsoleTextAttribute(h_console, 2);  // green
		SetConsoleTextAttribute(h_console, 6);  // yellow
		SetConsoleTextAttribute(h_console, 15);  // white
	}

	#pragma region error
	template<typename T>
	static void error(const T& arg) ERROR1BODY
	template<typename T>
	static void error(const LogContext& context, const T& str) ERROR2BODY
	template<typename ...TArgs>
	static void error(const std::string& pattern, const TArgs&... args) ERROR3BODY
	template<typename ...TArgs>
	static void error(const LogContext& context, const std::string& pattern, const TArgs&... args) ERROR4BODY
	#pragma endregion

	#pragma region warn
	template<typename T>
	static void warn(const T& arg) WARN1BODY
	template<typename T>
	static void warn(const LogContext& context, const T& str) WARN2BODY
	template<typename ...TArgs>
	static void warn(const std::string& pattern, const TArgs&... args) WARN3BODY
	template<typename ...TArgs>
	static void warn(const LogContext& context, const std::string& pattern, const TArgs&... args) WARN4BODY
	#pragma endregion

	#pragma region confirm
	template<typename T>
	static void confirm(const T& arg) CONFIRM1BODY
	template<typename T>
	static void confirm(const LogContext& context, const T& str) CONFIRM2BODY
	template<typename ...TArgs>
	static void confirm(const std::string& pattern, const TArgs&... args) CONFIRM3BODY
	template<typename ...TArgs>
	static void confirm(const LogContext& context, const std::string& pattern, const TArgs&... args) CONFIRM4BODY
	#pragma endregion

	#pragma region log
	template<typename T>
	static void log(const T& arg) LOG1BODY
	template<typename T>
	static void log(const LogContext& context, const T& str) LOG2BODY
	template<typename ...TArgs>
	static void log(const std::string& pattern, const TArgs&... args) LOG3BODY
	template<typename ...TArgs>
	static void log(const LogContext& context, const std::string& pattern, const TArgs&... args) LOG4BODY
	#pragma endregion
private:
	static HANDLE& get_console_handle() { 
		static HANDLE c_handle = GetStdHandle(STD_OUTPUT_HANDLE);
		return c_handle; 
	}
	static std::mutex& get_console_mutex() {
		static std::mutex c_mutex;
		return c_mutex;
	}

	#pragma region print
	static void print(const std::string& str, unsigned char colour = DEFUALT_CONSOLE_COLOUR) {
		LOG_LOCK();
		HANDLE h_console = Logger::get_console_handle();
		SetConsoleTextAttribute(h_console, colour);
		std::cout << str << '\n';
		SetConsoleTextAttribute(h_console, DEFUALT_CONSOLE_COLOUR);
		LOG_RELEASE();
	}
	static void max_print(const std::string& prefix, const std::string& str, int max_characters, unsigned char colour = DEFUALT_CONSOLE_COLOUR) {
		LOG_LOCK();
		HANDLE h_console = Logger::get_console_handle();
		SetConsoleTextAttribute(h_console, colour);
		std::cout << prefix;
		for (size_t i = 0; i < str.length(); i++) {
			if (i != 0) {
				if (i % (max_characters - prefix.size()) == 0) {
					std::cout << '\n';
					for (size_t j = 0; j < prefix.length() % max_characters; j++) std::cout << ' ';
				}
			}
			std::cout << str[i];
		}
		std::cout << '\n';
		SetConsoleTextAttribute(h_console, DEFUALT_CONSOLE_COLOUR);
		LOG_RELEASE();
	}
	#pragma endregion

	#pragma region pattern resolving
	template<typename ...TArgs>
	static std::string resolve_pattern(const std::string& pattern, const TArgs&... args) {
		std::string out = "";

		for (size_t i = 0; i < pattern.size(); i++) {
			if (pattern[i] == '{') {
				if (i < pattern.size() - 2) {
					size_t count = 0;
					for (size_t j = i + 1; j < pattern.size(); j++, count++)
						if (pattern[j] == '}') break;
					if (!count) { out += pattern[i]; continue; }
					std::string numstr = pattern.substr(i + 1, count);
					unsigned long index = std::stoul(numstr);
					out += Logger::get_arg(index, args...);
					i += count + 1;
				} else out += pattern[i];
			} else out += pattern[i];
		}

		return out;
	}

	template<typename T>
	static std::string get_arg(unsigned long index, const T& arg) {
		return std::to_string(arg);
	}
	template<typename T, typename ...TArgs>
	static std::string get_arg(unsigned long index, const T& arg, const TArgs&... args) {
		if (index == 0) return std::to_string(arg);
		return Logger::get_arg(index - 1, args...);
	}
	#pragma endregion
};