// Logging.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include "Logger.h"

class Vector2f : public ILoggable {
public:
	float x;
	float y;

	Vector2f(float x, float y) : x(x), y(y) {}

	Vector2f operator+(const Vector2f& other) {
		return Vector2f(x + other.x, y + other.y);
	}

	std::string to_string() {
		std::string a = std::to_string(x);
		return "(" + std::ftos(x) + ", " + std::ftos(y) + ")";
	}
};

int main() {
	Logger::init();
	Logger::error(LogContext{ true, 35 }, "100000000000000000000000000000000000000000000000000000000000000");
	Logger::confirm(true);
	std::cout << "please enter the first vector\n";
	float a, b;
	std::cin >> a;
	std::cin >> b;
	Vector2f v1(a, b);
	std::cout << "please enter the second vector\n";
	std::cin >> a;
	std::cin >> b;
	Vector2f v2(a, b);
	Logger::error("{0} + {1} = {2}", v1, v2, v1 + v2);

	system("PAUSE");
}