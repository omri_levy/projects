# Projects
This repository contains past projects, either completely finished or half finished, with all the source code and assosiated resources.

# Paint
The Paint project is a semi-advanced paint programme designed around the 8086 acrithecture, written in assembly language. It contains almost 3000 lines all in one file (paint.asm) with comments. It served as my project for year 10 in high school. Some of the features supported include: a colour palette, different shapes with a size meter that updates in real time to indicate the current time, a colour sampling tool, a bucket fill tool and the ability to save files.

Instructions:
    To launcch the paint application, launch Dosbox and start PAINT.EXE. It is recommended to set cycles=max for a smoother experience. A .bat file is also included as a shortcut but it requires the paint programme to be set in a specific location and for Dosbox to be in the PATH environment variable. Once the programme is launched, click the question mark to see help on different topic for more information.
    NOTE: in the same folder as PAINT.EXE there has to be a folder named "exports", in which all the exported files are stored

Deps:
    -Dosbox

# Tetris 3D
The Tetris 3D project was a fun idea I had to try and create a version of tetris where instead of the game being set in a 2D environment, the game takes place in 3D space, where there are more possible ways to rotate each piece and also move the camera around. Although I've never really finished the Tetris 3D game, I did experiment with some graphical features of Unity 3D and with Bosca Ceoil to create some music (based on the original Tetris music :D) for the game. As of now I have no plans of revisiting this project as the only features it's missing are a proper menu and high score system. This project is written using the Unity 3D game engine and C# programming language.

Instructions:
    To launch the game, double click the Tetris3D.exe executable (icon is currently the Unity icon). A little window should pop up, set the appropriate settings and the game shoudl start. You may pause the game at any point by clicking escape. To control the game, use Q and E to rotate the camera, use the arrow keys to rotate the pieces, use the spacebar to view the game surface from an above or press shifh to move the piece at a faster rate or tab to drop the piece.

# Chat
The chat project was an assignment I had to do between 11th and 12th grade, which I decided to take a few steps further and make it a proper app. I've implemented a users system, with a log-in/sign-up page when openning launching the chat. All the users are saved in a database managed using very basic Sqlite3 as the project is written in Python 2.7 and Sqlite3 seemed to be the easiest to pick up. I chose to work with the Kivy framework for creating the GUI and also some of the control flow using the Clock module for scheduling callbacks. I plan to revisit this project one day to clean up some of the code, looking back at it, it is very easy to make out that when I first started working on this project I had no idea of Kivy works and just did whatever I thought would work. I also hope to revisit the GUI and give the entire application a remake to make it look better, although I am happy with its current state as it is functional and the GUI isn't that big of an eyesore.

Instructions:
    To launch the chat, the server must first be launched, double click the server_main.py file to launch it. Once the server is up, launch application.py to create a new client. 
    NOTE: both server_main.py and application.py contain static IP addresses and ports, make sure they are set properly before launch!

Deps:
    -Python 2.7
    -Kivy

# Logger
The logger project is still work in progress. It is meant to be a single header file librally that provides simple and powerful logging functions for single threaded programmes. As of now it only contains the ability to format a log entry with a pattern passed as a string and multiple values thanks to variadic templates. I plan on adding the ability to create different log levels which can be set on and off using flags and also let different log levels have different colours, default log styles... If everything goes to plan, I hope I will also work on a multithreaded version.